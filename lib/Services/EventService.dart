import 'package:EventsApp/Constants/Environment.dart';
import 'package:http/http.dart' as http;

class EventService {
  //Fetch All Active Events
  Future<http.Response> getAllActiveEvents() async {
    return await http.get(Environment().apiurl + '/event/getAllActiveEvents');
  }

  //Fetch All NearBy Active Events
  Future<http.Response> getAllNearByActiveEvents(
      String userlatitude, String userLongitude, int range) async {
    return await http.get(Environment().apiurl + '/event/findNearby/' + userlatitude + '/' 
    + userLongitude + '/' + range.toString());
  }

  //Fetch All Deactive Events
  Future<http.Response> getAllDeactiveEvents() async {
    return await http.get(Environment().apiurl + '/event/getAllDeActiveEvents');
  }

  //Activate Event
  Future<http.Response> activateEventbyEventId(String id) async {
    return await http.get(Environment().apiurl + '/event/activateEvent/' + id);
  }
  

  //DeActivate Event
  Future<http.Response> deActivateEventbyEventId(String id) async {
    return await http.get(Environment().apiurl + '/event/deActivateEvent/' + id);
  }

  //Get Participated Users for Event
  Future<http.Response> getParticipatedUsersEventId(String id) async {
    return await http.get(Environment().apiurl + '/eventusermapping/getParticipatedUsers/' + id);
  }

  //Remove Participant from Event
  Future<http.Response> removeParticipantFromEvent(String userId,String eventId) async {
    return await http.get(Environment().apiurl + '/eventusermapping/removeParticipation/' + userId + '/' + eventId);
  }

  //Delete Event
  Future<http.Response> deleteEventbyEventId(String id) async {
    return await http.get(Environment().apiurl + '/event/deleteEvent/' + id);
  }

  //Add Participate in Event
  Future<http.Response> addPartcipantinEvent(String userid,String eventid) async{
    return await http.post(Environment().apiurl + '/eventusermapping/addParticipant/' + userid + '/' + eventid);
  }

  //check User Participation
  Future<http.Response>checkUserParticipation(String eventid,String userid)async {
    return await http.get(Environment().apiurl + '/eventusermapping/checkuserParticipation/' + eventid + '/' + userid);
  }

  //add Favorite
  Future<http.Response> addFavorite(String userid,String eventid) async{
    return await http.post(Environment().apiurl + '/event/addtoFavorites/' + userid + '/' + eventid);
  }

  //Remove Favorite
  Future<http.Response> removeFavorite(String userid,String eventid) async {
    return await http.post(Environment().apiurl + '/event/removeFromFavorites/' + userid + '/' + eventid);
  }

   //check User Favorites
  Future<http.Response>checkUserFavorites(String eventid,String userid) async {
    return await http.get(Environment().apiurl + '/user/checkUserFavEvents/' + eventid + '/' + userid);
  }

}
