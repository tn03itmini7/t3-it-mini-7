import 'dart:convert';

UserModel userModelFromJson(String str) => UserModel.fromJson(json.decode(str));

String userModelToJson(UserModel data) => json.encode(data.toJson());

class UserModel {
    UserModel({
        this.id,
        this.name,
        this.email,
        this.password,
        this.roles,
        this.permissions,
        this.favorites,
    });

    String id;
    String name;
    String email;
    String password;
    String roles;
    dynamic permissions;
    dynamic favorites;

    factory UserModel.fromJson(Map<String, dynamic> json) => UserModel(
        id: json["id"],
        name: json["name"],
        email: json["email"],
        password: json["password"],
        roles: json["roles"],
        permissions: json["permissions"],
        favorites: json["favorites"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "email": email,
        "password": password,
        "roles": roles,
        "permissions": permissions,
        "favorites": favorites,
    };
}