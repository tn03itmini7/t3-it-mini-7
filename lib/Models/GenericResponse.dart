class GenericResponse {
  String code;
  String message;
  bool body;

  GenericResponse({this.code, this.message, this.body});

  GenericResponse.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    message = json['message'];
    body = json['body'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['code'] = this.code;
    data['message'] = this.message;
    data['body'] = this.body;
    return data;
  }
}