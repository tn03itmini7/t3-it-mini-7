// To parse this JSON data, do
//
//     final eventModel = eventModelFromJson(jsonString);

import 'dart:convert';

EventModel eventModelFromJson(String str) => EventModel.fromJson(json.decode(str));

String eventModelToJson(EventModel data) => json.encode(data.toJson());

class EventModel {
    EventModel({
        this.id,
        this.name,
        this.active,
        this.description,
        this.coverimg,
        this.location,
        this.venue,
        this.date,
        this.eventheadname,
        this.eventheadphoto,
        this.createdon,
        this.publishedon,
    });

    String id;
    String name;
    bool active;
    String description;
    String coverimg;
    List<double> location;
    String venue;
    String date;
    String eventheadname;
    String eventheadphoto;
    dynamic createdon;
    dynamic publishedon;

    factory EventModel.fromJson(Map<String, dynamic> json) => EventModel(
        id: json["id"],
        name: json["name"],
        active: json["active"],
        description: json["description"],
        coverimg: json["coverimg"],
        location: List<double>.from(json["location"].map((x) => x.toDouble())),
        venue: json["venue"],
        date: json["date"],
        eventheadname: json["eventheadname"],
        eventheadphoto: json["eventheadphoto"],
        createdon: json["createdon"],
        publishedon: json["publishedon"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "active": active,
        "description": description,
        "coverimg": coverimg,
        "location": List<dynamic>.from(location.map((x) => x)),
        "venue": venue,
        "date": date,
        "eventheadname": eventheadname,
        "eventheadphoto": eventheadphoto,
        "createdon": createdon,
        "publishedon": publishedon,
    };
}