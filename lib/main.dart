import 'package:EventsApp/Views/Common/IntroductionScreen.dart';
import 'package:EventsApp/Views/Common/SplashScreen.dart';
import 'package:EventsApp/Views/Event/Admin/AdminBottomNavBarPageView.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

int initScreen;

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SharedPreferences prefs = await SharedPreferences.getInstance();
  initScreen = prefs.getInt('initScreen');
  await prefs.setInt('initScreen', 1);
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      initialRoute: initScreen == 0 || initScreen == null ? 'onboard':'splash',
      routes: {
        'onboard' : (context) => IntroScreenPageView(),
        'splash' : (context) => Splashscreen(),
        'adminnav' : (context) => AdminBottomNavBarPageView(),
      },
    );
  }
}
