import 'dart:convert';
import 'package:EventsApp/Services/UserService.dart';
import 'package:EventsApp/Views/Authentication/RegisterPageView.dart';
import 'package:EventsApp/Views/Event/Admin/AdminBottomNavBarPageView.dart';
import 'package:EventsApp/Views/Event/User/UserBottomNavBarPageView.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginPageView extends StatefulWidget {
  LoginPageView({Key key}) : super(key: key);

  @override
  _LoginPageViewState createState() => _LoginPageViewState();
}

class _LoginPageViewState extends State<LoginPageView> {
  //controllers to read input values
  TextEditingController emailcontroller = TextEditingController();
  TextEditingController passwordcontroller = TextEditingController();

  //booleans
  bool isLoading = false;
  bool _obscureText = true;

  // Toggles the password show status
  void _toggle() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }

  login(String email, String password) async {
    UserService userservice = new UserService();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      isLoading = true;
    });
    var result = await userservice.login(email, password);
    var parsedjson = json.decode(result.body);
    setState(() {
      isLoading = false;
    });
    if (parsedjson["name"] != null) {
      prefs.setString('role', parsedjson["roles"]);
      prefs.setString('userId', parsedjson["id"]);
      prefs.setBool('isLoggedIn', true);
      if (parsedjson["roles"] == 'User') {
        Get.offAll(() => UserBottomNavBarPageView());
      } else if (parsedjson["roles"] == 'ADMIN') {
        Get.offAll(() => AdminBottomNavBarPageView());
      } else {
        Get.offAll(() => LoginPageView());
      }
      Get.snackbar(
        "  Logged In Successfully",
        "  Welcome" + " " + parsedjson["name"],
        margin: EdgeInsets.all(10.0),
        colorText: Colors.black,
        snackPosition: SnackPosition.TOP,
        backgroundColor: Colors.grey[100],
        duration: Duration(seconds: 2),
        icon: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(10), bottomLeft: Radius.circular(10)),
            color: Colors.black,
          ),
          width: 150,
          height: 80,
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Center(
              child: Icon(
                Icons.verified_user_outlined,
                color: Color.fromARGB(255, 255, 208, 0),
              ),
            ),
          ),
        ),
      );
    } else {
      Get.snackbar(
        "  Please Enter Valid Credentials",
        "  Try again",
        margin: EdgeInsets.all(10.0),
        colorText: Colors.black,
        snackPosition: SnackPosition.TOP,
        backgroundColor: Colors.grey[100],
        duration: Duration(seconds: 2),
        icon: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(10), bottomLeft: Radius.circular(10)),
            color: Colors.black,
          ),
          width: 150,
          height: 80,
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Center(
              child: Icon(
                Icons.error_outline,
                color: Color.fromARGB(255, 255, 208, 0),
              ),
            ),
          ),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      resizeToAvoidBottomPadding: false,
      body: SingleChildScrollView(
        child: ModalProgressHUD(
          inAsyncCall: isLoading,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                child: Stack(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.fromLTRB(15.0, 80.0, 0.0, 0.0),
                      child: Text('Welcom To',
                          style: TextStyle(
                              color: Colors.blue[700],
                              fontSize: 40.0,
                              fontWeight: FontWeight.bold)),
                    ),
                    SizedBox(
                      height: 5.0,
                    ),
                    Container(
                      padding: EdgeInsets.fromLTRB(95.0, 140.0, 0.0, 0.0),
                      child: Text('Next In',
                          style: TextStyle(
                              color: Colors.blue[900],
                              fontSize: 50.0,
                              fontWeight: FontWeight.bold)),
                    ),
                  ],
                ),
              ),
              Container(
                  padding: EdgeInsets.only(top: 35.0, left: 20.0, right: 20.0),
                  child: Column(
                    children: <Widget>[
                      makeEmailInput(
                          label: "Enter Your Email", obscureText: true),
                      SizedBox(height: 5.0),
                      makePasswordInput(
                          label: "Enter Your Password", obscureText: true),
                      SizedBox(height: 5.0),
                      // Container(
                      //   alignment: Alignment(1.0, 0.0),
                      //   padding: EdgeInsets.only(top: 15.0, left: 20.0),
                      //   child: InkWell(
                      //     child: Text(
                      //       'Forgot Password',
                      //       style: TextStyle(
                      //           color: Colors.grey[600],
                      //           fontWeight: FontWeight.bold,
                      //           fontFamily: 'Montserrat',
                      //           decoration: TextDecoration.underline),
                      //     ),
                      //   ),
                      // ),
                      SizedBox(height: 40.0),
                      Container(
                          height: 60.0,
                          child: MaterialButton(
                            minWidth: double.infinity,
                            color: Colors.blue[900],
                            height: 60,
                            onPressed: () {
                              login(emailcontroller.text,
                                  passwordcontroller.text);
                            },
                            shape: RoundedRectangleBorder(
                                side: BorderSide(color: Colors.black),
                                borderRadius: BorderRadius.circular(10)),
                            child: Text(
                              "Login",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.w600,
                                  fontSize: 18),
                            ),
                          )),
                    ],
                  )),
              SizedBox(height: 30.0),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    'New Here ?',
                    style: TextStyle(fontFamily: 'Montserrat'),
                  ),
                  SizedBox(width: 5.0),
                  InkWell(
                    onTap: () {
                      Get.to(() => RegisterPageView());
                    },
                    child: Text(
                      'Register',
                      style: TextStyle(
                          color: Colors.black,
                          fontFamily: 'Montserrat',
                          fontWeight: FontWeight.bold,
                          decoration: TextDecoration.underline),
                    ),
                  )
                ],
              ),
              SizedBox(height: 30.0),
            ],
          ),
        ),
      ),
    );
  }

  Widget makePasswordInput({label, obscureText = false}) {
    return Form(
      onChanged: () {},
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            label,
            style: TextStyle(
                fontSize: 15,
                fontWeight: FontWeight.w400,
                color: Colors.black87),
          ),
          SizedBox(
            height: 5,
          ),
          TextFormField(
            controller: passwordcontroller,
            obscureText: _obscureText,
            decoration: InputDecoration(
              prefixIcon: Icon(
                Icons.admin_panel_settings,
                color: Colors.black,
              ),
              suffixIcon: IconButton(
                  icon: _obscureText
                      ? Icon(
                          Icons.visibility,
                          color: Colors.black,
                        )
                      : Icon(
                          Icons.visibility_off,
                          color: Colors.black,
                        ),
                  onPressed: () {
                    _toggle();
                  }),
              contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
              enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.black)),
              border: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.black)),
            ),
          ),
          SizedBox(
            height: 30,
          ),
        ],
      ),
    );
  }

  Widget makeEmailInput({label, obscureText = false}) {
    return Form(
      onChanged: () {},
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            label,
            style: TextStyle(
                fontSize: 15,
                fontWeight: FontWeight.w400,
                color: Colors.black87),
          ),
          SizedBox(
            height: 5,
          ),
          TextFormField(
            controller: emailcontroller,
            decoration: InputDecoration(
              prefixIcon: Icon(
                Icons.email_outlined,
                color: Colors.black,
              ),
              contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
              enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.black)),
              border: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.black)),
            ),
          ),
          SizedBox(
            height: 30,
          ),
        ],
      ),
    );
  }
}
