import 'package:EventsApp/Services/UserService.dart';
import 'package:EventsApp/Views/Authentication/LoginPageView.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';

class RegisterPageView extends StatefulWidget {
  RegisterPageView({Key key}) : super(key: key);

  @override
  _RegisterPageViewState createState() => _RegisterPageViewState();
}

class _RegisterPageViewState extends State<RegisterPageView> {
  bool _obscureText = true;
  final emailController = TextEditingController();
  final passController = TextEditingController();
  final nameController = TextEditingController();
  bool isloading = false;

  // Toggles the password show status
  void _toggle() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }

  signupUser(String name, String email, String pass) async {
    setState(() {
      isloading = true;
    });
    UserService userService = new UserService();
    var res = await userService.signup(name, email, pass);
    if (res.statusCode == 200) {
      setState(() {
        isloading = false;
      });
      Get.snackbar(
        "  Registered Successfully",
        "  Success  ",
        margin: EdgeInsets.all(10.0),
        colorText: Colors.black,
        snackPosition: SnackPosition.TOP,
        backgroundColor: Colors.grey[100],
        duration: Duration(seconds: 2),
        icon: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(10), bottomLeft: Radius.circular(10)),
            color: Colors.black,
          ),
          width: 150,
          height: 80,
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Center(
              child: Icon(
                Icons.verified_user_outlined,
                color: Color.fromARGB(255, 255, 208, 0),
              ),
            ),
          ),
        ),
      );
      Get.offAll(() => LoginPageView());
    } else {
      setState(() {
        isloading = false;
      });
      Get.snackbar(
        "  Error in Registeration ",
        "  Try again",
        margin: EdgeInsets.all(10.0),
        colorText: Colors.black,
        snackPosition: SnackPosition.TOP,
        backgroundColor: Colors.grey[100],
        duration: Duration(seconds: 2),
        icon: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(10), bottomLeft: Radius.circular(10)),
            color: Colors.black,
          ),
          width: 150,
          height: 80,
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Center(
              child: Icon(
                Icons.error_outline,
                color: Color.fromARGB(255, 255, 208, 0),
              ),
            ),
          ),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        resizeToAvoidBottomPadding: false,
        body: SingleChildScrollView(
          child: ModalProgressHUD(
            inAsyncCall: isloading,
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    child: Stack(
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.fromLTRB(75.0, 80.0, 0.0, 0.0),
                          child: Text(
                            'Do Register',
                            style: TextStyle(
                                color: Colors.blue[900],
                                fontSize: 40.0,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                      padding:
                          EdgeInsets.only(top: 40.0, left: 15.0, right: 15.0),
                      child: Column(
                        children: <Widget>[
                          makeNameInput(
                              label: "Enter Your Name", obscureText: false),
                          SizedBox(height: 30.0),
                          makeemailInput(
                              label: "Enter Your Email", obscureText: false),
                          SizedBox(height: 5.0),
                          makePasswordInput(
                              label: "Enter Your Password", obscureText: true),
                          SizedBox(height: 40.0),
                          Container(
                              height: 50.0,
                              child: MaterialButton(
                                minWidth: double.infinity,
                                color: Colors.blue[900],
                                height: 60,
                                onPressed: () async {
                                  await signupUser(
                                      nameController.text,
                                      emailController.text,
                                      passController.text);
                                },
                                shape: RoundedRectangleBorder(
                                    side: BorderSide(color: Colors.black),
                                    borderRadius: BorderRadius.circular(10)),
                                child: Text(
                                  "Sign Up",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.w600,
                                      fontSize: 18),
                                ),
                              )),
                          SizedBox(height: 20.0),
                          Container(
                            height: 50.0,
                            color: Colors.transparent,
                            child: Container(
                              decoration: BoxDecoration(
                                  border: Border.all(
                                      color: Colors.black,
                                      style: BorderStyle.solid,
                                      width: 1.0),
                                  color: Colors.transparent,
                                  borderRadius: BorderRadius.circular(10.0)),
                              child: InkWell(
                                onTap: () {
                                  Navigator.of(context).pop();
                                },
                                child: Center(
                                  child: Text('Go Back',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontFamily: 'Montserrat')),
                                ),
                              ),
                            ),
                          ),
                          SizedBox(height: 20.0),
                        ],
                      )),
                ]),
          ),
        ));
  }

  Widget makeNameInput({label, obscureText = false}) {
    return Form(
      onChanged: () {},
      autovalidateMode: AutovalidateMode.always,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            label,
            style: TextStyle(
                fontSize: 15,
                fontWeight: FontWeight.w400,
                color: Colors.black87),
          ),
          SizedBox(
            height: 5,
          ),
          TextFormField(
            controller: nameController,
            keyboardType: TextInputType.text,
            obscureText: obscureText,
            decoration: InputDecoration(
              prefixIcon: Icon(Icons.verified_user_outlined),
              contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
              enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.black)),
              border: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.black)),
            ),
          ),
        ],
      ),
    );
  }

  Widget makePasswordInput({label, obscureText = false}) {
    return Form(
      onChanged: () {},
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            label,
            style: TextStyle(
                fontSize: 15,
                fontWeight: FontWeight.w400,
                color: Colors.black87),
          ),
          SizedBox(
            height: 5,
          ),
          TextFormField(
            controller: passController,
            obscureText: _obscureText,
            decoration: InputDecoration(
              prefixIcon: Icon(
                Icons.admin_panel_settings_outlined,
                color: Colors.black,
              ),
              suffixIcon: IconButton(
                  icon: _obscureText
                      ? Icon(
                          Icons.visibility,
                          color: Colors.black,
                        )
                      : Icon(
                          Icons.visibility_off,
                          color: Colors.black,
                        ),
                  onPressed: () {
                    _toggle();
                  }),
              contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
              enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.black)),
              border: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.black)),
            ),
          ),
          SizedBox(
            height: 30,
          ),
        ],
      ),
    );
  }

  Widget makeemailInput({label, obscureText = false}) {
    return Form(
      onChanged: () {},
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            label,
            style: TextStyle(
                fontSize: 15,
                fontWeight: FontWeight.w400,
                color: Colors.black87),
          ),
          SizedBox(
            height: 5,
          ),
          TextFormField(
            controller: emailController,
            obscureText: obscureText,
            keyboardType: TextInputType.emailAddress,
            decoration: InputDecoration(
              prefixIcon: Icon(
                Icons.email_outlined,
                color: Colors.black,
              ),
              contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
              enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.black)),
              border: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.black)),
            ),
          ),
          SizedBox(
            height: 30,
          ),
        ],
      ),
    );
  }
}
