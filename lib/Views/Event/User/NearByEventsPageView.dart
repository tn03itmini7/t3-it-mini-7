import 'dart:async';
import 'dart:convert';

import 'package:EventsApp/Models/EventModel.dart';
import 'package:EventsApp/Services/EventService.dart';
import 'package:EventsApp/Views/Common/NoItems.dart';
import 'package:EventsApp/Views/Event/Skeleton/ExploreEventSkeletonView.dart';
import 'package:flutter/services.dart';
import 'package:geocoder/geocoder.dart';
import 'package:intl/intl.dart';
import 'package:location/location.dart';
import 'package:flutter/material.dart';

class NearByEventsPageView extends StatefulWidget {
  NearByEventsPageView({Key key}) : super(key: key);

  @override
  _NearByEventsPageViewState createState() => _NearByEventsPageViewState();
}

class _NearByEventsPageViewState extends State<NearByEventsPageView> {
  List<EventModel> _events = List<EventModel>();
  LocationData _currentLocation;
  StreamSubscription<LocationData> _locationSubscription;
  var _locationService = new Location();
  String error;
  String _currentAddress = "";
  String userlatitude;
  String userLongitude;
  bool _isLoading = false;
  DateTime date = DateTime.now();

  @override
  void initState() {
    setState(() {
      _isLoading = true;
    });
    initPlatformState();
    super.initState();
  }

  getNearByActiveEvents(String latitude, String longitude, int range) async {
    EventService eventService = new EventService();
    var result = await eventService.getAllNearByActiveEvents(
        userlatitude, userLongitude, 10);
    var events = List<EventModel>();
    if (result.statusCode == 200) {
      setState(() {
        _isLoading = false;
      });
      var eventslistJson = json.decode(result.body);
      for (var event in eventslistJson) {
        events.add(EventModel.fromJson(event));
      }
      return events;
    } else {
      return null;
    }
  }

  void initPlatformState() async {
    try {
      _currentLocation = await _locationService.getLocation();
    } on PlatformException catch (e) {
      if (e.code == 'PERMISSION_DENIED') {
        error = 'Permission denied';
      } else if (e.code == "PERMISSION_DENIED_NEVER_ASK") {
        error = 'Permission denied';
      }
      _currentLocation = null;
    }
    _getAddressFromLatLng();
  }

  _getAddressFromLatLng() async {
    final coordinates =
        new Coordinates(_currentLocation.latitude, _currentLocation.longitude);
    var addresses =
        await Geocoder.local.findAddressesFromCoordinates(coordinates);
    var first = addresses.first;
    _currentAddress = " ${first.locality} , ${first.postalCode}";
    setState(() {
      userlatitude = coordinates.latitude.toString();
      userLongitude = coordinates.longitude.toString();
    });
    getNearByActiveEvents(userlatitude, userLongitude, 10).then((value) => {
          if (this.mounted)
            {
              setState(() {
                _events.addAll(value);
              }),
            }
        });
    return coordinates;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: AppBar(
        title: Text(
          "NearBy Events",
          style: TextStyle(color: Colors.white),
        ),
        backgroundColor: Colors.blue[900],
        elevation: 0,
        brightness: Brightness.light,
        leading: Icon(
          Icons.location_on_outlined,
          color: Colors.white,
          size: 25,
        ),
        centerTitle: true,
        actions: <Widget>[
          Padding(
            padding: EdgeInsets.only(right: 20.0),
            child: Container(
              width: 35,
              height: 35,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  image: DecorationImage(
                      image: AssetImage('assets/images/Splash.png'),
                      fit: BoxFit.cover)),
              child: Transform.translate(
                offset: Offset(15, -15),
                child: Container(
                  margin: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                      border: Border.all(width: 3, color: Colors.white),
                      shape: BoxShape.circle,
                      color: Colors.yellow[800]),
                ),
              ),
            ),
          )
        ],
      ),
      body: _isLoading
          ? ExploreEventSkeletonView()
          : _events.isEmpty
              ? NoItemsFoundWidget().noNearByEventsFoundWidget()
              : SafeArea(
                  child: SingleChildScrollView(
                    padding: EdgeInsets.all(10),
                    child: Container(
                      child: Column(
                        children: [
                          // Container(
                          //   padding: EdgeInsets.symmetric(vertical: 2),
                          //   decoration: BoxDecoration(
                          //       borderRadius: BorderRadius.circular(10),
                          //       color: Colors.white,
                          //       border:
                          //         Border.all(color: Colors.black, width: 2.0)),
                          //   child: TextField(
                          //     decoration: InputDecoration(
                          //         border: InputBorder.none,
                          //         prefixIcon: Icon(
                          //           Icons.search,
                          //           color: Colors.grey,
                          //         ),
                          //         hintText: "Search Near By Events",
                          //         hintStyle: TextStyle(color: Colors.grey)),
                          //   ),
                          // ),
                          SizedBox(
                            height: 5.0,
                          ),
                          ListView.builder(
                              physics: NeverScrollableScrollPhysics(),
                              itemCount: _events.length,
                              scrollDirection: Axis.vertical,
                              shrinkWrap: true,
                              itemBuilder: (BuildContext context, index) {
                                return Padding(
                                  padding: const EdgeInsets.only(
                                      top: 5.0, bottom: 5.0),
                                  child: makeItem(
                                    image: _events[index].coverimg.isNotEmpty
                                        ? _events[index].coverimg
                                        : CircularProgressIndicator,
                                    name: _events[index].name,
                                    date: DateFormat.MMMd().format(
                                        DateTime.parse(_events[index].date)),
                                  ),
                                );
                              }),
                        ],
                      ),
                    ),
                  ),
                ),
    );
  }

  Widget makeItem({image, name, date}) {
    return Row(
      children: <Widget>[
        Container(
          width: 35,
          height: 200,
          margin: EdgeInsets.only(right: 5.0),
          child: Column(
            children: <Widget>[
              Text(
                date,
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 18,
                    fontWeight: FontWeight.bold),
              ),
            ],
          ),
        ),
        Expanded(
          child: Container(
            height: 200,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                image: DecorationImage(
                    image: NetworkImage(image), fit: BoxFit.cover)),
            child: Container(
              padding: EdgeInsets.all(20),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  gradient: LinearGradient(colors: [
                    Colors.black.withOpacity(.4),
                    Colors.black.withOpacity(.1),
                  ])),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Text(
                    name,
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 25,
                        fontWeight: FontWeight.bold),
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}
