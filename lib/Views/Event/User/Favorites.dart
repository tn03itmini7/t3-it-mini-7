import 'dart:convert';

import 'package:EventsApp/Models/EventModel.dart';
import 'package:EventsApp/Services/UserService.dart';
import 'package:EventsApp/Views/Common/NoItems.dart';
import 'package:EventsApp/Views/Event/Skeleton/ExploreEventSkeletonView.dart';
import 'package:EventsApp/Views/Event/User/EventDetailPageView.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';

class FavoritesEventsPage extends StatefulWidget {
  FavoritesEventsPage({Key key}) : super(key: key);

  @override
  _FavoritesEventsPageState createState() => _FavoritesEventsPageState();
}

class _FavoritesEventsPageState extends State<FavoritesEventsPage> {
  List<EventModel> event;
  List<EventModel> _events = List<EventModel>();
  bool _isLoading = false;

  getUserFavorites() async {
    print('getting called');
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var uid = prefs.getString('userId');
    UserService userService = new UserService();
    var res = await userService.getUserFavorites(uid);
    if (res.statusCode == 200) {
      setState(() {
        _isLoading = false;
      });
      var events = List<EventModel>();
      var parsedjson = json.decode(res.body);
      var evelist = parsedjson["body"] as List;

      event =
          evelist.map<EventModel>((json) => EventModel.fromJson(json)).toList();

      for (var event in evelist) {
        events.add(EventModel.fromJson(event));
      }
      return events;
    } else {
      setState(() {
        _isLoading = false;
      });
    }
  }

  @override
  void initState() {
    setState(() {
      _isLoading = true;
    });
    super.initState();
    getUserFavorites().then((value) => {
          setState(() {
            _events.addAll(value);
          })
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Favorite Events",
          style: TextStyle(color: Colors.white),
        ),
        backgroundColor: Colors.blue[900],
        elevation: 0,
        brightness: Brightness.light,
        leading: Icon(
          Icons.favorite,
          color: Colors.white,
          size: 25,
        ),
        centerTitle: true,
        actions: <Widget>[
          Padding(
            padding: EdgeInsets.only(right: 20.0),
            child: Container(
              width: 35,
              height: 35,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  image: DecorationImage(
                      image: AssetImage('assets/images/Splash.png'),
                      fit: BoxFit.cover)),
              child: Transform.translate(
                offset: Offset(15, -15),
                child: Container(
                  margin: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                      border: Border.all(width: 3, color: Colors.white),
                      shape: BoxShape.circle,
                      color: Colors.yellow[800]),
                ),
              ),
            ),
          )
        ],
      ),
      body: _isLoading
          ? ExploreEventSkeletonView()
          : _events.isEmpty
              ? NoItemsFoundWidget().noEventsFoundWidget()
              : SafeArea(
                  child: SingleChildScrollView(
                    padding: EdgeInsets.all(10),
                    child: Container(
                      child: Column(
                        children: [
                          SizedBox(
                            height: 5.0,
                          ),
                          ListView.builder(
                              physics: NeverScrollableScrollPhysics(),
                              itemCount: _events.length,
                              scrollDirection: Axis.vertical,
                              shrinkWrap: true,
                              itemBuilder: (BuildContext context, index) {
                                return Padding(
                                  padding: const EdgeInsets.only(
                                      top: 5.0, bottom: 5.0),
                                  child: InkWell(
                                    onTap: () {
                                      Get.to(() =>
                                          DetailPage(event: _events[index]));
                                    },
                                    child: makeItem(
                                      image: _events[index].coverimg.isNotEmpty
                                          ? _events[index].coverimg
                                          : CircularProgressIndicator,
                                      name: _events[index].name,
                                      date: DateFormat.MMMd().format(
                                          DateTime.parse(_events[index].date)),
                                    ),
                                  ),
                                );
                              }),
                        ],
                      ),
                    ),
                  ),
                ),
    );
  }

  Widget makeItem({image, name, date}) {
    return Row(
      children: <Widget>[
        Container(
          width: 35,
          height: 200,
          margin: EdgeInsets.only(right: 5.0),
          child: Column(
            children: <Widget>[
              Text(
                date,
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 18,
                    fontWeight: FontWeight.bold),
              ),
            ],
          ),
        ),
        Expanded(
          child: Container(
            height: 200,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                image: DecorationImage(
                    image: NetworkImage(image), fit: BoxFit.cover)),
            child: Container(
              padding: EdgeInsets.all(20),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  gradient: LinearGradient(colors: [
                    Colors.black.withOpacity(.3),
                    Colors.black.withOpacity(.2),
                  ])),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Text(
                    name,
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 25,
                        fontWeight: FontWeight.bold),
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}
