import 'package:EventsApp/Views/Event/User/ExploreEvents.dart';
import 'package:EventsApp/Views/Event/User/Favorites.dart';
import 'package:EventsApp/Views/Event/User/NearByEventsPageView.dart';
import 'package:EventsApp/Views/Event/User/UserProfilePageView.dart';
import 'package:flutter/material.dart';

class UserBottomNavBarPageView extends StatefulWidget {
  UserBottomNavBarPageView({Key key}) : super(key: key);

  @override
  _UserBottomNavBarPageViewState createState() =>
      _UserBottomNavBarPageViewState();
}

class _UserBottomNavBarPageViewState extends State<UserBottomNavBarPageView> {
  int _selectedIndex = 0;

  void _onItemTap(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  List<Widget> _buildScreens = [
    ExploreEventsPage(),
    NearByEventsPageView(),
    FavoritesEventsPage(),
    UserProfilePAgeView(),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Material(
          child: _buildScreens.elementAt(_selectedIndex),
        ),
        bottomNavigationBar: BottomNavigationBar(
          selectedItemColor: Colors.white,
          unselectedItemColor: Colors.grey[400],
          items: [
            BottomNavigationBarItem(
              backgroundColor: Colors.blue[900],
              icon: Icon(Icons.explore_outlined),
              label: "Explore Events",
            ),
            BottomNavigationBarItem(
              backgroundColor: Colors.blue[900],
              icon: Icon(Icons.pin_drop),
              label: "NearBy Events",
            ),
            BottomNavigationBarItem(
              backgroundColor: Colors.blue[900],
              icon: Icon(Icons.favorite),
              label: "Favorites",
            ),
            BottomNavigationBarItem(
              backgroundColor: Colors.blue[900],
              icon: Icon(Icons.settings),
              label: "Settings",
            ),
          ],
          currentIndex: _selectedIndex,
          onTap: _onItemTap,
        ));
  }
}
