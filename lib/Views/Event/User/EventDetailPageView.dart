import 'dart:convert';
import 'package:EventsApp/Models/EventModel.dart';
import 'package:EventsApp/Models/GenericResponse.dart';
import 'package:EventsApp/Services/EventService.dart';
import 'package:flutter/material.dart';
import 'package:geocoder/geocoder.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

class DetailPage extends StatefulWidget {
  final EventModel event;
  DetailPage({Key key, @required this.event}) : super(key: key);
  @override
  _DetailPageState createState() => _DetailPageState();
}

class _DetailPageState extends State<DetailPage> {
  bool isPartcipated = false;
  bool isfavorite = false;
  bool checkButtonStatus = false;
  String _currentAddress = " ";
  String userId;
  ProgressDialog progressDialog;

  addParticipant(String eventid) async {
    progressDialog.show();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var uid = prefs.getString('userId');
    setState(() {
      userId = uid;
    });
    EventService eventService = new EventService();
    var res = await eventService.addPartcipantinEvent(uid, eventid);
    print(res.body);
    if (res.statusCode == 200) {
      progressDialog.hide().whenComplete(() => {
            Get.snackbar(
              "   Paricipation in Event Successful",
              "   Success ",
              margin: EdgeInsets.all(10.0),
              colorText: Colors.black,
              snackPosition: SnackPosition.TOP,
              backgroundColor: Colors.grey[100],
              duration: Duration(seconds: 2),
              icon: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(10),
                      bottomLeft: Radius.circular(10)),
                  color: Colors.black,
                ),
                width: 150,
                height: 80,
                child: Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Center(
                    child: Icon(
                      Icons.done_outline_rounded,
                      color: Color.fromARGB(255, 255, 208, 0),
                    ),
                  ),
                ),
              ),
            ),
          });
    } else {
      progressDialog.hide().whenComplete(() => {
            Get.snackbar(
              "  Error Participating in Event",
              "  Please Try Later",
              margin: EdgeInsets.all(10.0),
              colorText: Colors.black,
              snackPosition: SnackPosition.TOP,
              backgroundColor: Colors.grey[100],
              duration: Duration(seconds: 2),
              icon: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(10),
                      bottomLeft: Radius.circular(10)),
                  color: Colors.black,
                ),
                width: 150,
                height: 80,
                child: Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Center(
                    child: Icon(
                      Icons.error_outline,
                      color: Color.fromARGB(255, 255, 208, 0),
                    ),
                  ),
                ),
              ),
            ),
          });
    }
  }

  Future<bool> checkUserParticipation() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var uid = prefs.getString('userId');
    EventService eventService = new EventService();
    var res = await eventService.checkUserParticipation(widget.event.id, uid);
    final Map parsedJson = json.decode(res.body);
    final responseCall = GenericResponse.fromJson(parsedJson);
    if (responseCall.body) {
      setState(() {
        checkButtonStatus = !checkButtonStatus;
      });
      return true;
    } else {
      checkButtonStatus = false;
      return false;
    }
  }

  Future<bool> chechUserFavEvents() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var uid = prefs.getString('userId');
    var res = await http.get(
        'https://eventmgmtapp.herokuapp.com/user/checkUserFavEvents/' +
            widget.event.id +
            '/' +
            uid);
    final Map parsedJson = json.decode(res.body);
    final responseCall = GenericResponse.fromJson(parsedJson);
    if (responseCall.body) {
      setState(() {
        isfavorite = !isfavorite;
      });
      return true;
    } else {
      isfavorite = false;
      return false;
    }
  }

  addFavorite() async {
    progressDialog.show();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var uid = prefs.getString('userId');
    setState(() {
      userId = uid;
    });
    EventService eventService = new EventService();
    var res = await eventService.addFavorite(uid, widget.event.id);
    print(res.body);
    if (res.statusCode == 200) {
      progressDialog.hide().whenComplete(() => {
            Get.snackbar(
              "   Added to Favorites",
              "   Success ",
              margin: EdgeInsets.all(10.0),
              colorText: Colors.black,
              snackPosition: SnackPosition.TOP,
              backgroundColor: Colors.grey[100],
              duration: Duration(seconds: 2),
              icon: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(10),
                      bottomLeft: Radius.circular(10)),
                  color: Colors.black,
                ),
                width: 150,
                height: 80,
                child: Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Center(
                    child: Icon(
                      Icons.done_outline_rounded,
                      color: Color.fromARGB(255, 255, 208, 0),
                    ),
                  ),
                ),
              ),
            ),
          });
      setState(() {
        isfavorite = !isfavorite;
      });
    } else {
      progressDialog.hide().whenComplete(() => {
            Get.snackbar(
              "  Error Adding Favorite",
              "  Please Try Later",
              margin: EdgeInsets.all(10.0),
              colorText: Colors.black,
              snackPosition: SnackPosition.TOP,
              backgroundColor: Colors.grey[100],
              duration: Duration(seconds: 2),
              icon: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(10),
                      bottomLeft: Radius.circular(10)),
                  color: Colors.black,
                ),
                width: 150,
                height: 80,
                child: Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Center(
                    child: Icon(
                      Icons.error_outline,
                      color: Color.fromARGB(255, 255, 208, 0),
                    ),
                  ),
                ),
              ),
            ),
          });
    }
  }

  removeFavorite() async {
    progressDialog.show();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var uid = prefs.getString('userId');
    setState(() {
      userId = uid;
    });
    EventService eventService = new EventService();
    var res = await eventService.removeFavorite(uid, widget.event.id);
    if (res.statusCode == 200) {
      progressDialog.hide().whenComplete(() => {
            Get.snackbar(
              "   Removed From Favorites",
              "   Success ",
              margin: EdgeInsets.all(10.0),
              colorText: Colors.black,
              snackPosition: SnackPosition.TOP,
              backgroundColor: Colors.grey[100],
              duration: Duration(seconds: 2),
              icon: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(10),
                      bottomLeft: Radius.circular(10)),
                  color: Colors.black,
                ),
                width: 150,
                height: 80,
                child: Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Center(
                    child: Icon(
                      Icons.done_outline_rounded,
                      color: Color.fromARGB(255, 255, 208, 0),
                    ),
                  ),
                ),
              ),
            ),
          });
    } else {
      progressDialog.hide().whenComplete(() => {
            Get.snackbar(
              "  Error Removing From Favorites",
              "  Please Try Later",
              margin: EdgeInsets.all(10.0),
              colorText: Colors.black,
              snackPosition: SnackPosition.TOP,
              backgroundColor: Colors.grey[100],
              duration: Duration(seconds: 2),
              icon: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(10),
                      bottomLeft: Radius.circular(10)),
                  color: Colors.black,
                ),
                width: 150,
                height: 80,
                child: Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Center(
                    child: Icon(
                      Icons.error_outline,
                      color: Color.fromARGB(255, 255, 208, 0),
                    ),
                  ),
                ),
              ),
            )
          });
    }
  }

  _getAddressFromLatLng() async {
    final coordinates = new Coordinates(
        widget.event.location.first, widget.event.location.last);
    var addresses =
        await Geocoder.local.findAddressesFromCoordinates(coordinates);
    var first = addresses.first;
    _currentAddress =
        " ${first.subLocality} ${first.locality} ${first.postalCode}";
    setState(() {});
    return _currentAddress;
  }

  @override
  void initState() {
    super.initState();
    _getAddressFromLatLng();
    checkUserParticipation();
    chechUserFavEvents();
  }

  @override
  Widget build(BuildContext context) {
    progressDialog =
        new ProgressDialog(context, type: ProgressDialogType.Normal);
    progressDialog.style(
      message: 'Please Wait...',
      progressWidget: Container(
          padding: EdgeInsets.all(15.0),
          child: SizedBox(
            height: 10.0,
            width: 10.0,
            child: CircularProgressIndicator(
              valueColor: new AlwaysStoppedAnimation<Color>(Colors.blue[900]),
            ),
          )),
      maxProgress: 100.0,
      progressTextStyle: TextStyle(
          color: Colors.blue[900], fontSize: 13.0, fontWeight: FontWeight.w400),
    );
    Size size = MediaQuery.of(context).size;
    return Scaffold(
        backgroundColor: Colors.blue[900],
        body: SingleChildScrollView(
          child: Column(children: <Widget>[
            Container(
              color: Colors.blue[900],
              height: MediaQuery.of(context).size.shortestSide * 0.9,
              child: Stack(
                children: <Widget>[
                  ClipPath(
                    clipper: MyCustomClipper(),
                    child: Container(
                      height: MediaQuery.of(context).size.shortestSide * 1.2,
                      decoration: BoxDecoration(
                        gradient: LinearGradient(colors: [
                          Colors.black.withOpacity(.5),
                          Colors.black.withOpacity(.5),
                        ]),
                        image: DecorationImage(
                          image: NetworkImage(widget.event.coverimg),
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ),
                  Align(
                    alignment: Alignment(0, 1),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Container(
                          decoration: new BoxDecoration(
                            boxShadow: [
                              new BoxShadow(
                                color: Colors.black12,
                                blurRadius: 8.0,
                              ),
                            ],
                          ),
                          width: MediaQuery.of(context).size.shortestSide / 1.3,
                          height: MediaQuery.of(context).size.shortestSide / 7,
                          child: Card(
                            elevation: 0.8,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(5.0),
                            ),
                            child: Container(
                              child: Center(
                                child: Text(
                                  widget.event.name,
                                  style: TextStyle(
                                    fontSize: 20.0,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(height: 45),
                      ],
                    ),
                  ),
                  TopBar(),
                ],
              ),
            ),
            Container(
              color: Colors.blue[900],
              child: Container(
                  child: Padding(
                      padding:
                          EdgeInsets.only(top: 10.0, right: 15.0, left: 15.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              CircleAvatar(
                                radius: 40,
                                backgroundImage:
                                    NetworkImage(widget.event.eventheadphoto),
                              ),
                              Center(
                                child: Column(
                                  children: [
                                    Text(
                                      widget.event.eventheadname,
                                      style: TextStyle(
                                          fontSize: 23.0,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.white),
                                    ),
                                    Text(
                                      "Event Head",
                                      style: TextStyle(
                                          fontSize: 18.0,
                                          color: Colors.grey[200]),
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),
                          SizedBox(height: 10.0),
                          Divider(color: Colors.white),
                          SizedBox(height: 10.0),
                          Padding(
                            padding: const EdgeInsets.only(left: 25.0),
                            child: Container(
                              child: Column(
                                children: [
                                  Row(
                                    children: [
                                      Icon(
                                        Icons.location_on,
                                        color: Colors.white,
                                        size: 20,
                                      ),
                                      SizedBox(width: 12.0),
                                      Text(
                                        _currentAddress,
                                        style: TextStyle(
                                            fontSize: 18, color: Colors.white),
                                      ),
                                    ],
                                  ),
                                  SizedBox(height: 20.0),
                                  Row(
                                    children: [
                                      Icon(
                                        Icons.calendar_today,
                                        color: Colors.white,
                                        size: 20,
                                      ),
                                      SizedBox(width: 15.0),
                                      Text(
                                        DateFormat.yMMMEd().format(
                                            DateTime.parse(widget.event.date)),
                                        style: TextStyle(
                                            fontSize: 17, color: Colors.white),
                                      ),
                                    ],
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Divider(color: Colors.white),
                          SizedBox(
                            height: 10,
                          ),
                          Container(
                            alignment: Alignment.topLeft,
                            child: Column(
                              children: [
                                Container(
                                  alignment: Alignment.topLeft,
                                  child: Text(
                                    "Description",
                                    style: TextStyle(
                                        fontSize: 20.0, color: Colors.white),
                                  ),
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Text(
                                  widget.event.description,
                                  style: TextStyle(
                                      fontSize: 18.0, color: Colors.grey[200]),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 30,
                          ),
                          Container(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(left: 20.0),
                                  child: Center(
                                    child: SizedBox(
                                        width: MediaQuery.of(context)
                                                .size
                                                .shortestSide /
                                            1.7,
                                        child: checkButtonStatus == true
                                            ? Container(
                                                height: 50,
                                                child: RaisedButton(
                                                  onPressed: null,
                                                  shape: RoundedRectangleBorder(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              10.0)),
                                                  color: Colors.grey,
                                                  child: Text(
                                                    "Participated",
                                                    style: TextStyle(
                                                        color: Colors.white,
                                                        fontSize: 18.0,
                                                        letterSpacing: 1.5),
                                                  ),
                                                  disabledColor: Colors.black12,
                                                  disabledElevation: 1,
                                                  disabledTextColor:
                                                      Colors.black,
                                                ),
                                              )
                                            : Container(
                                                height: 50,
                                                child: RaisedButton(
                                                  onPressed: () async {
                                                    await addParticipant(
                                                        widget.event.id);
                                                    setState(() {
                                                      checkButtonStatus =
                                                          !checkButtonStatus;
                                                    });
                                                  },
                                                  shape: RoundedRectangleBorder(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              10.0)),
                                                  color: Colors.white,
                                                  child: Text(
                                                    "Participate",
                                                    style: TextStyle(
                                                        color: Colors.blue[900],
                                                        fontSize: 18.0,
                                                        letterSpacing: 1.5),
                                                  ),
                                                ),
                                              )),
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(left: 20.0),
                                  child: Container(
                                      child: isfavorite == true
                                          ? makeRemoveFavButton()
                                          : makefavButton()),
                                )
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 30,
                          ),
                        ],
                      ))),
            ),
          ]),
        ));
  }

  Widget makefavButton() {
    return IconButton(
        iconSize: 35,
        color: Colors.redAccent[400],
        icon: Icon(Icons.favorite_border),
        tooltip: 'Add to Favorite',
        onPressed: () async {
          await addFavorite();
        });
  }

  Widget makeRemoveFavButton() {
    return IconButton(
        iconSize: 35,
        color: Colors.redAccent[400],
        icon: Icon(Icons.favorite),
        tooltip: 'Remove from Favorite',
        onPressed: () async {
          await removeFavorite();
          setState(() {
            isfavorite = !isfavorite;
          });
        });
  }
}

class MyCustomClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path path = Path();
    path.lineTo(size.width, 0);
    path.lineTo(size.width, size.height - 150);
    path.lineTo(0, size.height);
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return true;
  }
}

class TopBar extends StatelessWidget {
  const TopBar({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          IconButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            icon: Icon(
              Icons.arrow_back,
              color: Colors.blue[900],
            ),
          ),
        ],
      ),
    );
  }
}
