import 'package:EventsApp/Models/EventModel.dart';
import 'package:EventsApp/Models/UserModel.dart';
import 'package:EventsApp/Views/Authentication/LoginPageView.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

class UserProfilePAgeView extends StatefulWidget {
  UserProfilePAgeView({Key key}) : super(key: key);

  @override
  _UserProfilePAgeViewState createState() => _UserProfilePAgeViewState();
}

class _UserProfilePAgeViewState extends State<UserProfilePAgeView> {
  //clear user Preferences
  clearpreferences() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool('isLoggedIn', false);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Profile",
          style: TextStyle(color: Colors.white),
        ),
        backgroundColor: Colors.blue[900],
        elevation: 0,
        brightness: Brightness.light,
        leading: Icon(
          Icons.supervised_user_circle,
          color: Colors.white,
          size: 28,
        ),
        centerTitle: true,
        actions: <Widget>[
          Padding(
            padding: EdgeInsets.only(right: 20.0),
            child: Container(
              width: 35,
              height: 35,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  image: DecorationImage(
                      image: AssetImage('assets/images/Splash.png'),
                      fit: BoxFit.cover)),
              child: Transform.translate(
                offset: Offset(15, -15),
                child: Container(
                  margin: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                      border: Border.all(width: 3, color: Colors.white),
                      shape: BoxShape.circle,
                      color: Colors.yellow[800]),
                ),
              ),
            ),
          )
        ],
      ),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(15.0),
          child: MaterialButton(
            minWidth: double.infinity,
            color: Colors.blue[900],
            height: 60,
            onPressed: () async {
              await clearpreferences();
              Get.offAll(() => LoginPageView());
            },
            shape: RoundedRectangleBorder(
                side: BorderSide(color: Colors.black),
                borderRadius: BorderRadius.circular(10)),
            child: Text(
              "Logout",
              style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.w600,
                  fontSize: 18),
            ),
          ),
        ),
      ),
    );
  }
}

// class UserProfilePAgeView extends StatefulWidget {
//   final EventModel event;
//   UserProfilePAgeView({Key key, @required this.event}) : super(key: key);

//   @override
//   _UserProfilePAgeViewState createState() => _UserProfilePAgeViewState();
// }

// class _UserProfilePAgeViewState extends State<UserProfilePAgeView> {
//   List<UserModel> _users = List<UserModel>();
//   String eventId;

//   //clear user Preferences
//   clearpreferences() async {
//     SharedPreferences prefs = await SharedPreferences.getInstance();
//     prefs.setBool('isLoggedIn', false);
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: Text("Profile"),
//         backgroundColor: Colors.lightBlue[900],
//       ),
//       body: SingleChildScrollView(
//         padding: EdgeInsets.symmetric(vertical: 20),
//         child: Column(
//           children: [
//             ProfilePic(),
//             Info(
//               name: "_user",
//             ),
//             SizedBox(height: 40),
//             ProfileMenu(
//               text: "Contact Us",
//               press: () {},
//             ),
//             ProfileMenu(
//               text: "Feedback",
//               press: () {},
//             ),
//             ProfileMenu(
//               text: "Log Out",
//               press: () async {
//                 await clearpreferences();
//                 Get.offAll(() => LoginPageView());
//               },
//             ),
//           ],
//         ),
//       ),
//     );
//   }
// }

// class ProfileMenu extends StatelessWidget {
//   const ProfileMenu({
//     Key key,
//     @required this.text,
//     this.press,
//   }) : super(key: key);

//   final String text;
//   final VoidCallback press;

//   @override
//   Widget build(BuildContext context) {
//     return Padding(
//       padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
//       child: FlatButton(
//         padding: EdgeInsets.all(20),
//         shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30)),
//         color: Colors.grey[300],
//         onPressed: press,
//         child: Row(
//           children: [
//             SizedBox(width: 20),
//             Expanded(child: Text(text)),
//             Icon(Icons.arrow_forward_ios),
//           ],
//         ),
//       ),
//     );
//   }
// }

// class ProfilePic extends StatelessWidget {
//   const ProfilePic({
//     Key key,
//   }) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return SizedBox(
//       height: 115,
//       width: 115,
//       child: Stack(
//         fit: StackFit.expand,
//         overflow: Overflow.visible,
//         children: [
//           CircleAvatar(
//             backgroundColor: Colors.white,
//             backgroundImage: AssetImage("assets/images/profile.png"),
//           ),
//           // Positioned(
//           //   right: -16,
//           //   bottom: 0,
//           //   child: SizedBox(
//           //     height: 46,
//           //     width: 46,
//           //     child: FlatButton(
//           //       shape: RoundedRectangleBorder(
//           //         borderRadius: BorderRadius.circular(50),
//           //         side: BorderSide(color: Colors.white),
//           //       ),
//           //       color: Color(0xFFF5F6F9),
//           //       onPressed: () {},
//           //       child: Container(),
//           //     ),
//           //   ),
//           // )
//         ],
//       ),
//     );
//   }
// }

// class Info extends StatelessWidget {
//   const Info({
//     Key key,
//     this.name,
//     this.email,
//   }) : super(key: key);
//   final String name, email;

//   @override
//   Widget build(BuildContext context) {
//     return SizedBox(
//       height: 80,
//       child: Stack(
//         children: <Widget>[
//           Center(
//             child: Column(
//               mainAxisAlignment: MainAxisAlignment.end,
//               children: <Widget>[
//                 Text(
//                   "Welcome ",
//                   style: TextStyle(
//                     fontSize: 22, // 22
//                     color: Colors.black,
//                   ),
//                 ), //5
//                 // Text(
//                 //   email,
//                 //   style: TextStyle(
//                 //     fontWeight: FontWeight.w400,
//                 //     color: Color(0xFF8492A2),
//                 //   ),
//                 // )
//               ],
//             ),
//           )
//         ],
//       ),
//     );
//   }
// }
