
import 'package:EventsApp/animation/FadeAnimation.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class ExploreEventSkeletonView extends StatefulWidget {
  ExploreEventSkeletonView({Key key}) : super(key: key);

  @override
  _ExploreEventSkeletonViewState createState() =>
      _ExploreEventSkeletonViewState();
}

class _ExploreEventSkeletonViewState extends State<ExploreEventSkeletonView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[100],
      body: SafeArea(
        child: SingleChildScrollView(
          padding: EdgeInsets.all(20),
          child: Container(
            child: Column(
              children: [
                FadeAnimation(
                    0,
                    Shimmer.fromColors(
                      baseColor: Colors.grey[300],
                      highlightColor: Colors.white,
                      child: Container(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: Colors.white,
                            border:
                                Border.all(color: Colors.black, width: 2.0)),
                        child: TextField(
                          decoration: InputDecoration(
                              border: InputBorder.none,
                              prefixIcon: Icon(
                                Icons.search,
                                color: Colors.grey,
                              ),
                              hintText: "Search Event",
                              hintStyle: TextStyle(color: Colors.grey)),
                        ),
                      ),
                    )),
                SizedBox(
                  height: 10.0,
                ),
                FadeAnimation(
                  0,
                  ListView.builder(
                      physics: NeverScrollableScrollPhysics(),
                      itemCount: 10,
                      scrollDirection: Axis.vertical,
                      shrinkWrap: true,
                      itemBuilder: (BuildContext context, index) {
                        return Padding(
                          padding: const EdgeInsets.only(top: 10.0),
                          child: makeItem(),
                        );
                      }),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget makeItem({image, name, date}) {
    return Row(
      children: <Widget>[
        Shimmer.fromColors(
          baseColor: Colors.grey[300],
          highlightColor: Colors.white,
          child: Container(
            width: 30,
            height: 200,
            margin: EdgeInsets.only(right: 5.0),
          ),
        ),
        Expanded(
            child: Shimmer.fromColors(
                child: Container(
                  height: 200,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: Container(
                    padding: EdgeInsets.all(20),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        gradient: LinearGradient(colors: [
                          Colors.black.withOpacity(.4),
                          Colors.black.withOpacity(.1),
                        ])),
                  ),
                ),
                baseColor: Colors.grey[400],
                highlightColor: Colors.white)),
      ],
    );
  }
}
