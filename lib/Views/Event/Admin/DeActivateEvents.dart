import 'dart:convert';

import 'package:EventsApp/Models/EventModel.dart';
import 'package:EventsApp/Services/EventService.dart';
import 'package:EventsApp/Views/Common/NoItems.dart';
import 'package:EventsApp/Views/Event/Admin/ViewParticipationPageView.dart';
import 'package:EventsApp/Views/Event/Skeleton/ExploreEventSkeletonView.dart';
import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:progress_dialog/progress_dialog.dart';

class DeActiavteEventPageView extends StatefulWidget {
  DeActiavteEventPageView({Key key}) : super(key: key);

  @override
  _DeActiavteEventPageViewState createState() =>
      _DeActiavteEventPageViewState();
}

class _DeActiavteEventPageViewState extends State<DeActiavteEventPageView> {
  bool _isLoading = false;
  List<EventModel> _events = List<EventModel>();
  DateTime date = DateTime.now();
  String eventId;
  ProgressDialog progressDialog;

  Future<List<EventModel>> getAllActiveEvents() async {
    setState(() {
      _isLoading = true;
    });
    EventService eventService = new EventService();
    var res = await eventService.getAllActiveEvents();
    var events = List<EventModel>();
    if (res.statusCode == 200) {
      setState(() {
        _isLoading = false;
      });
      var eventslistJson = json.decode(res.body);
      for (var event in eventslistJson) {
        events.add(EventModel.fromJson(event));
      }
      return events;
    } else {
      setState(() {
        _isLoading = false;
      });
      return null;
    }
  }

  deleteEvent(String id) async {
    EventService eventService = new EventService();
    var res = await eventService.deleteEventbyEventId(id);
    if (res.statusCode != 200) {
      Get.snackbar(
        "   Event Deleted Successfully",
        "   Success ",
        margin: EdgeInsets.all(10.0),
        colorText: Colors.black,
        snackPosition: SnackPosition.TOP,
        backgroundColor: Colors.grey[100],
        duration: Duration(seconds: 2),
        icon: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(10), bottomLeft: Radius.circular(10)),
            color: Colors.black,
          ),
          width: 150,
          height: 80,
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Center(
              child: Icon(
                Icons.done_outline_rounded,
                color: Color.fromARGB(255, 255, 208, 0),
              ),
            ),
          ),
        ),
      );
    } else {
      Get.snackbar(
        "  Error in Deleting Event",
        "  Please Try Later",
        margin: EdgeInsets.all(10.0),
        colorText: Colors.black,
        snackPosition: SnackPosition.TOP,
        backgroundColor: Colors.grey[100],
        duration: Duration(seconds: 2),
        icon: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(10), bottomLeft: Radius.circular(10)),
            color: Colors.black,
          ),
          width: 150,
          height: 80,
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Center(
              child: Icon(
                Icons.error_outline,
                color: Color.fromARGB(255, 255, 208, 0),
              ),
            ),
          ),
        ),
      );
    }
  }

  deActivateEvent(String id) async {
    progressDialog.show();
    EventService eventService = new EventService();
    var res = await eventService.deActivateEventbyEventId(id);
    if (res.statusCode == 200) {
      progressDialog.hide().whenComplete(() => {
            Get.snackbar(
              "   Event Deactivated Successfully",
              "   Success ",
              margin: EdgeInsets.all(10.0),
              colorText: Colors.black,
              snackPosition: SnackPosition.TOP,
              backgroundColor: Colors.grey[100],
              duration: Duration(seconds: 2),
              icon: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(10),
                      bottomLeft: Radius.circular(10)),
                  color: Colors.black,
                ),
                width: 150,
                height: 80,
                child: Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Center(
                    child: Icon(
                      Icons.done_outline_rounded,
                      color: Color.fromARGB(255, 255, 208, 0),
                    ),
                  ),
                ),
              ),
            ),
          });
    } else {
      progressDialog.hide().whenComplete(() => {
            Get.snackbar(
              "  Error in Deactivating Event",
              "  Please Try Later",
              margin: EdgeInsets.all(10.0),
              colorText: Colors.black,
              snackPosition: SnackPosition.TOP,
              backgroundColor: Colors.grey[100],
              duration: Duration(seconds: 2),
              icon: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(10),
                      bottomLeft: Radius.circular(10)),
                  color: Colors.black,
                ),
                width: 150,
                height: 80,
                child: Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Center(
                    child: Icon(
                      Icons.error_outline,
                      color: Color.fromARGB(255, 255, 208, 0),
                    ),
                  ),
                ),
              ),
            ),
          });
    }
  }

  @override
  void initState() {
    getAllActiveEvents().then((value) => {
          setState(() {
            _events.addAll(value);
          })
        });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    progressDialog =
        new ProgressDialog(context, type: ProgressDialogType.Normal);
    progressDialog.style(
      message: 'Deactivating Event Please Wait...',
      progressWidget: Container(
          padding: EdgeInsets.all(15.0),
          child: SizedBox(
            height: 10.0,
            width: 10.0,
            child: CircularProgressIndicator(
              valueColor: new AlwaysStoppedAnimation<Color>(Colors.blue[900]),
            ),
          )),
      maxProgress: 100.0,
      progressTextStyle: TextStyle(
          color: Colors.blue[900], fontSize: 13.0, fontWeight: FontWeight.w400),
    );
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "DeActivate Events",
          style: TextStyle(color: Colors.white),
        ),
        backgroundColor: Colors.blue[900],
        elevation: 0,
        brightness: Brightness.light,
        leading: Icon(
          Icons.menu,
          color: Colors.white,
        ),
        actions: <Widget>[
          Padding(
            padding: EdgeInsets.only(right: 20.0),
            child: Container(
              width: 35,
              height: 35,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  image: DecorationImage(
                      image: AssetImage('assets/images/Splash.png'),
                      fit: BoxFit.cover)),
              child: Transform.translate(
                offset: Offset(15, -15),
                child: Container(
                  margin: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                      border: Border.all(width: 3, color: Colors.white),
                      shape: BoxShape.circle,
                      color: Colors.yellow[800]),
                ),
              ),
            ),
          )
        ],
      ),
      body: _isLoading
          ? ExploreEventSkeletonView()
          : _events.isEmpty
              ? NoItemsFoundWidget().noActiveEventsFoundWidget()
              : SafeArea(
                  child: SingleChildScrollView(
                    padding: EdgeInsets.all(10),
                    child: Container(
                      child: Column(
                        children: [
                          // Container(
                          //   padding: EdgeInsets.symmetric(vertical: 2),
                          //   decoration: BoxDecoration(
                          //       borderRadius: BorderRadius.circular(10),
                          //       color: Colors.white,
                          //       border: Border.all(
                          //           color: Colors.black, width: 2.0)),
                          //   child: TextField(
                          //     decoration: InputDecoration(
                          //         border: InputBorder.none,
                          //         prefixIcon: Icon(
                          //           Icons.search,
                          //           color: Colors.grey,
                          //         ),
                          //         hintText: "Search Event",
                          //         hintStyle: TextStyle(color: Colors.grey)),
                          //   ),
                          // ),
                          SizedBox(
                            height: 5.0,
                          ),
                          ListView.builder(
                              physics: NeverScrollableScrollPhysics(),
                              itemCount: _events.length,
                              scrollDirection: Axis.vertical,
                              shrinkWrap: true,
                              itemBuilder: (BuildContext context, index) {
                                return Padding(
                                  padding: const EdgeInsets.only(
                                      top: 5.0, bottom: 5.0),
                                  child: Column(
                                    children: [
                                      Row(
                                        children: <Widget>[
                                          Container(
                                            width: 35,
                                            height: 200,
                                            margin: EdgeInsets.only(right: 5.0),
                                            child: Column(
                                              children: <Widget>[
                                                Text(
                                                  DateFormat.MMMd().format(
                                                      DateTime.parse(
                                                          _events[index].date)),
                                                  style: TextStyle(
                                                      color: Colors.black,
                                                      fontSize: 18,
                                                      fontWeight:
                                                          FontWeight.bold),
                                                ),
                                              ],
                                            ),
                                          ),
                                          Expanded(
                                            child: Container(
                                              height: 200,
                                              decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.circular(10),
                                                  image: DecorationImage(
                                                      image: _events[index]
                                                              .coverimg
                                                              .isNotEmpty
                                                          ? NetworkImage(
                                                              _events[index]
                                                                  .coverimg)
                                                          : CircularProgressIndicator,
                                                      fit: BoxFit.cover)),
                                              child: Container(
                                                padding: EdgeInsets.all(20),
                                                decoration: BoxDecoration(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            10),
                                                    gradient:
                                                        LinearGradient(colors: [
                                                      Colors.black
                                                          .withOpacity(.4),
                                                      Colors.black
                                                          .withOpacity(.1),
                                                    ])),
                                                child: Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.end,
                                                  children: <Widget>[
                                                    Text(
                                                      _events[index].name,
                                                      style: TextStyle(
                                                          color: Colors.white,
                                                          fontSize: 25,
                                                          fontWeight:
                                                              FontWeight.bold),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                      Container(
                                        color: Colors.grey[300],
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceAround,
                                          children: [
                                            ButtonBar(
                                              alignment:
                                                  MainAxisAlignment.start,
                                              children: <Widget>[
                                                MaterialButton(
                                                  elevation: 8.0,
                                                  minWidth: 40,
                                                  color: Colors.blue[900],
                                                  height: 52,
                                                  onPressed: () async {
                                                    await deActivateEvent(
                                                        _events[index].id);
                                                    setState(() {
                                                      _events.removeAt(index);
                                                    });
                                                  },
                                                  shape: RoundedRectangleBorder(
                                                      side: BorderSide(
                                                          color: Colors.black),
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              10)),
                                                  child: Text(
                                                    "DeActivate Event",
                                                    style: TextStyle(
                                                        color: Colors.white,
                                                        fontWeight:
                                                            FontWeight.w600,
                                                        fontSize: 14),
                                                  ),
                                                ),
                                                MaterialButton(
                                                  elevation: 8.0,
                                                  minWidth: 40,
                                                  color: Colors.blue[900],
                                                  height: 52,
                                                  onPressed: () {
                                                    Get.to(() =>
                                                        ViewParticipationPageView(
                                                          event: _events[index],
                                                        ));
                                                  },
                                                  shape: RoundedRectangleBorder(
                                                      side: BorderSide(
                                                          color: Colors.black),
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              10)),
                                                  child: Text(
                                                    "View Participation",
                                                    style: TextStyle(
                                                        color: Colors.white,
                                                        fontWeight:
                                                            FontWeight.w600,
                                                        fontSize: 14),
                                                  ),
                                                ),
                                                Container(
                                                    width: 40,
                                                    decoration: BoxDecoration(
                                                        color: Colors
                                                            .lightBlue[900],
                                                        shape: BoxShape.circle),
                                                    child: IconButton(
                                                        icon: Icon(
                                                          Icons.delete,
                                                          color: Colors.white,
                                                        ),
                                                        onPressed: () {
                                                          AwesomeDialog(
                                                              context: context,
                                                              dialogType:
                                                                  DialogType
                                                                      .WARNING,
                                                              headerAnimationLoop:
                                                                  false,
                                                              animType: AnimType
                                                                  .TOPSLIDE,
                                                              title: 'Warn',
                                                              desc:
                                                                  'This Event is Active. Are You Sure you want to Delete this Event ?',
                                                              btnOkText:
                                                                  'Delete',
                                                              btnCancelOnPress:
                                                                  () {},
                                                              btnOkOnPress:
                                                                  () async {
                                                                await deleteEvent(
                                                                  _events[index]
                                                                      .id,
                                                                );
                                                                setState(() {
                                                                  _events
                                                                      .removeAt(
                                                                          index);
                                                                });
                                                              }).show();
                                                        }))
                                              ],
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                );
                              }),
                        ],
                      ),
                    ),
                  ),
                ),
    );
  }
}
