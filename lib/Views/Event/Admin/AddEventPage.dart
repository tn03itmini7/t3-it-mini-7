import 'dart:convert';

import 'package:EventsApp/Views/Event/Admin/AdminBottomNavBarPageView.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_map_location_picker/google_map_location_picker.dart';
import 'package:image_picker/image_picker.dart';
import 'package:http/http.dart' as http;
import 'dart:io';

import 'package:progress_dialog/progress_dialog.dart';

class AddEventPageView extends StatefulWidget {
  AddEventPageView({Key key}) : super(key: key);

  @override
  _AddEventPageViewState createState() => _AddEventPageViewState();
}

class _AddEventPageViewState extends State<AddEventPageView> {
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  ProgressDialog progressDialog;
  LocationResult _pickedLocation;
  String eventname;
  String eventinfo;
  String eventheadname;
  File _headimage;
  File _coverimage;
  String headimageUrl;
  String coverimageUrl;
  String latitude;
  String longitude;
  String eventlatitude;
  DateTime date;
  String eventlongitude;

  final eventnamecontroller = TextEditingController();
  final eventinfocontroller = TextEditingController();
  final eventheadnamecontroller = TextEditingController();

  Future<http.Response> createEvent(
      String eventname, String eventinfo, String eventheadname) async {
    progressDialog.show();
    var res = await http.post(
      'https://eventmgmtapp.herokuapp.com/event/add',
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, Object>{
        'name': eventname,
        'description': eventinfo,
        'coverimg': coverimageUrl,
        'location': [double.parse(eventlatitude), double.parse(eventlongitude)],
        'date': date.toIso8601String(), //_datainfo.toIso8601String(),
        'eventheadname': eventheadname,
        'eventheadphoto': headimageUrl,
      }),
    );
    if (res.statusCode == 200) {
      progressDialog.hide().whenComplete(() => {
            Get.snackbar(
              "   Event Added Successfully",
              "   Success ",
              margin: EdgeInsets.all(10.0),
              colorText: Colors.black,
              snackPosition: SnackPosition.TOP,
              backgroundColor: Colors.grey[100],
              duration: Duration(seconds: 2),
              icon: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(10),
                      bottomLeft: Radius.circular(10)),
                  color: Colors.black,
                ),
                width: 150,
                height: 80,
                child: Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Center(
                    child: Icon(
                      Icons.done_outline_rounded,
                      color: Color.fromARGB(255, 255, 208, 0),
                    ),
                  ),
                ),
              ),
            ),
          });
      Get.offAll(() => AdminBottomNavBarPageView());
    } else {
      progressDialog.hide();
      Get.snackbar(
        "  Error in Adding Event",
        "  Please Try Later",
        margin: EdgeInsets.all(10.0),
        colorText: Colors.black,
        snackPosition: SnackPosition.TOP,
        backgroundColor: Colors.grey[100],
        duration: Duration(seconds: 2),
        icon: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(10), bottomLeft: Radius.circular(10)),
            color: Colors.black,
          ),
          width: 150,
          height: 80,
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Center(
              child: Icon(
                Icons.error_outline,
                color: Color.fromARGB(255, 255, 208, 0),
              ),
            ),
          ),
        ),
      );
    }
  }

  Future<String> uploadHeadImage(_headimage) async {
    String fileName = DateTime.now().millisecondsSinceEpoch.toString();
    StorageReference storageReference =
        FirebaseStorage.instance.ref().child("Head_Images").child(fileName);
    StorageUploadTask uploadTask = storageReference.putFile(_headimage);
    StorageTaskSnapshot taskSnapshot = await uploadTask.onComplete;
    String downloadUrl = await taskSnapshot.ref.getDownloadURL();
    setState(() {
      headimageUrl = downloadUrl;
    });
    return downloadUrl;
  }

  Future<String> uploadCoverImage(_coverimage) async {
    String fileName = DateTime.now().millisecondsSinceEpoch.toString();
    StorageReference storageReference =
        FirebaseStorage.instance.ref().child("Cover_Images").child(fileName);
    StorageUploadTask uploadTask = storageReference.putFile(_coverimage);
    StorageTaskSnapshot taskSnapshot = await uploadTask.onComplete;
    String coverdownloadUrl = await taskSnapshot.ref.getDownloadURL();
    setState(() {
      coverimageUrl = coverdownloadUrl;
    });
    return coverdownloadUrl;
  }

  Future getHeadImage() async {
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);
    setState(() {
      _headimage = image;
    });
  }

  Future getCoverImage() async {
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);
    setState(() {
      _coverimage = image;
    });
  }

  Future<TimeOfDay> _selectTime(BuildContext context) {
    final now = DateTime.now();
    return showTimePicker(
      context: context,
      initialTime: TimeOfDay(hour: now.hour, minute: now.minute),
    );
  }

  Future<DateTime> _selectDateTime(BuildContext context) => showDatePicker(
        context: context,
        initialDate: DateTime.now().add(Duration(seconds: 1)),
        firstDate: DateTime.now(),
        lastDate: DateTime(2100),
      );

  final form = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    progressDialog =
        new ProgressDialog(context, type: ProgressDialogType.Normal);
    progressDialog.style(
      message: 'Adding Event Please Wait...',
      progressWidget: Container(
          padding: EdgeInsets.all(15.0),
          child: SizedBox(
            height: 10.0,
            width: 10.0,
            child: CircularProgressIndicator(
              valueColor: new AlwaysStoppedAnimation<Color>(Colors.blue[900]),
            ),
          )),
      maxProgress: 100.0,
      progressTextStyle: TextStyle(
          color: Colors.blue[900], fontSize: 13.0, fontWeight: FontWeight.w400),
    );

    return Scaffold(
      key: scaffoldKey,
      appBar: AppBar(
        title: Text(
          "Add Event",
          style: TextStyle(color: Colors.white),
        ),
        backgroundColor: Colors.blue[900],
        elevation: 0,
        brightness: Brightness.light,
        leading: Icon(
          Icons.menu,
          color: Colors.white,
        ),
        actions: <Widget>[
          Padding(
            padding: EdgeInsets.only(right: 20.0),
            child: Container(
              width: 35,
              height: 35,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  image: DecorationImage(
                      image: AssetImage('assets/images/Splash.png'),
                      fit: BoxFit.cover)),
              child: Transform.translate(
                offset: Offset(15, -15),
                child: Container(
                  margin: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                      border: Border.all(width: 3, color: Colors.white),
                      shape: BoxShape.circle,
                      color: Colors.yellow[800]),
                ),
              ),
            ),
          )
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.all(8),
              child: Material(
                elevation: 5.0,
                clipBehavior: Clip.antiAlias,
                borderRadius: BorderRadius.circular(8),
                child: Form(
                  key: form,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      AppBar(
                        leading: Icon(Icons.add),
                        elevation: 0,
                        title: Text('Event Details'),
                        backgroundColor: Colors.blue[900],
                        centerTitle: true,
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 16, right: 16, top: 16),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              "Enter Event Name",
                              style: TextStyle(
                                  fontSize: 15,
                                  fontWeight: FontWeight.w400,
                                  color: Colors.black87),
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            TextFormField(
                              controller: eventnamecontroller,
                              keyboardType: TextInputType.text,
                              decoration: InputDecoration(
                                contentPadding: EdgeInsets.symmetric(
                                    vertical: 0, horizontal: 10),
                                enabledBorder: OutlineInputBorder(
                                    borderSide:
                                        BorderSide(color: Colors.black)),
                                border: OutlineInputBorder(
                                    borderSide:
                                        BorderSide(color: Colors.black)),
                              ),
                              onChanged: (value) {
                                this.eventname = value;
                              },
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                            right: 10.0, left: 10.0, bottom: 10.0, top: 10.0),
                        child: Divider(
                          indent: 5.0,
                          thickness: 1.0,
                          color: Colors.grey[500],
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 16, right: 16, top: 2.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              "Enter Event Description",
                              style: TextStyle(
                                  fontSize: 15,
                                  fontWeight: FontWeight.w400,
                                  color: Colors.black87),
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            TextFormField(
                              controller: eventinfocontroller,
                              keyboardType: TextInputType.multiline,
                              maxLines: 4,
                              maxLength: 180,
                              decoration: InputDecoration(
                                contentPadding: EdgeInsets.symmetric(
                                    vertical: 0, horizontal: 10),
                                enabledBorder: OutlineInputBorder(
                                    borderSide:
                                        BorderSide(color: Colors.black)),
                                border: OutlineInputBorder(
                                    borderSide:
                                        BorderSide(color: Colors.black)),
                              ),
                              onChanged: (value) {
                                this.eventinfo = value;
                              },
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                            right: 10.0, left: 10.0, bottom: 10.0, top: 10.0),
                        child: Divider(
                          indent: 5.0,
                          thickness: 1.0,
                          color: Colors.grey[500],
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 16, right: 16, top: 2.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              "Enter Event Head Name",
                              style: TextStyle(
                                  fontSize: 15,
                                  fontWeight: FontWeight.w400,
                                  color: Colors.black87),
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            TextFormField(
                              controller: eventheadnamecontroller,
                              keyboardType: TextInputType.name,
                              decoration: InputDecoration(
                                contentPadding: EdgeInsets.symmetric(
                                    vertical: 0, horizontal: 10),
                                enabledBorder: OutlineInputBorder(
                                    borderSide:
                                        BorderSide(color: Colors.black)),
                                border: OutlineInputBorder(
                                    borderSide:
                                        BorderSide(color: Colors.black)),
                              ),
                              onChanged: (value) {
                                this.eventheadname = value;
                              },
                            ),
                            SizedBox(
                              height: 20.0,
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Material(
                elevation: 5.0,
                clipBehavior: Clip.antiAlias,
                borderRadius: BorderRadius.circular(8),
                child: Form(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      AppBar(
                        automaticallyImplyLeading: false,
                        elevation: 0,
                        title: Text('Event Location & Image Details'),
                        backgroundColor: Colors.blue[900],
                        centerTitle: true,
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 16, right: 16, top: 16),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              "Select Event Venue",
                              style: TextStyle(
                                  fontSize: 15,
                                  fontWeight: FontWeight.w500,
                                  color: Colors.black),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Container(
                              height: 48,
                              child: _pickedLocation == null
                                  ? Text(
                                      " Location is not Yet Selected ",
                                      style: TextStyle(
                                          fontWeight: FontWeight.w400,
                                          fontSize: 15),
                                    )
                                  : Column(
                                      children: [
                                        Align(
                                          alignment: Alignment.topLeft,
                                          child: Text(
                                            "Selected Location is :",
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold),
                                          ),
                                        ),
                                        Text(
                                          _pickedLocation.latLng.toString(),
                                        ),
                                      ],
                                    ),
                            ),
                            ButtonBar(
                              children: <Widget>[
                                RaisedButton(
                                  onPressed: () async {
                                    LocationResult result =
                                        await showLocationPicker(
                                      context,
                                      'AIzaSyC0OLDh6R955glZw0-Qt45d2wtXZGKvE4M',
                                      myLocationButtonEnabled: true,
                                      layersButtonEnabled: true,
                                    );
                                    setState(() => _pickedLocation = result);
                                    var location = result.latLng.toString();
                                    location.substring(0);
                                    final split = location.split(',');
                                    final Map<int, String> values = {
                                      for (int i = 0; i < split.length; i++)
                                        i: split[i]
                                    };
                                    final value1 = values[0];
                                    final value2 = values[1];
                                    setState(() {
                                      latitude = value1;
                                      var lat = latitude.split("(");
                                      eventlatitude = lat.elementAt(1);
                                      eventlatitude.trim();
                                      longitude = value2;
                                      var long = longitude.split(")");
                                      eventlongitude = long.elementAt(0);
                                      eventlongitude.trim();
                                    });
                                  },
                                  color: Colors.blue[900],
                                  child: Text("Select Event Venue"),
                                  elevation: 8.0,
                                ),
                                RaisedButton(
                                  onPressed: () {
                                    setState(() {
                                      _pickedLocation = null;
                                    });
                                  },
                                  color: Colors.red[500],
                                  child: Text("Cancel Selection"),
                                  elevation: 8.0,
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                            right: 10.0, left: 10.0, bottom: 5.0),
                        child: Divider(
                          indent: 5.0,
                          thickness: 1.0,
                          color: Colors.grey[500],
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 16, right: 16, top: 2.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              "Select Event Cover Image",
                              style: TextStyle(
                                  fontSize: 15,
                                  fontWeight: FontWeight.w500,
                                  color: Colors.black87),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Container(
                              height: 48,
                              child: _coverimage == null
                                  ? Text(
                                      " Cover Image is not Yet Selected ",
                                      style: TextStyle(
                                          fontWeight: FontWeight.w400,
                                          fontSize: 15),
                                    )
                                  : Column(
                                      children: [
                                        Align(
                                          alignment: Alignment.topLeft,
                                          child: Text(
                                            "Selected Cover Image is :",
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold),
                                          ),
                                        ),
                                        Text(
                                          _coverimage.path.toString(),
                                        ),
                                      ],
                                    ),
                            ),
                            ButtonBar(
                              children: <Widget>[
                                RaisedButton(
                                  onPressed: () {
                                    getCoverImage();
                                  },
                                  color: Colors.blue[900],
                                  child: Text("Select Cover Image"),
                                  elevation: 8.0,
                                ),
                                RaisedButton(
                                  onPressed: () {
                                    setState(() {
                                      _coverimage = null;
                                    });
                                  },
                                  color: Colors.red[500],
                                  child: Text("Cancel Selection"),
                                  elevation: 8.0,
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                            right: 10.0, left: 10.0, bottom: 5.0),
                        child: Divider(
                          indent: 5.0,
                          thickness: 1.0,
                          color: Colors.grey[500],
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 16, right: 16, top: 2.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              "Select Event Head Image",
                              style: TextStyle(
                                  fontSize: 15,
                                  fontWeight: FontWeight.w500,
                                  color: Colors.black87),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Container(
                              height: 48,
                              child: _headimage == null
                                  ? Text(
                                      " Head Image is not Yet Selected ",
                                      style: TextStyle(
                                          fontWeight: FontWeight.w400,
                                          fontSize: 15),
                                    )
                                  : Column(
                                      children: [
                                        Align(
                                          alignment: Alignment.topLeft,
                                          child: Text(
                                            "Selected Head Image is :",
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold),
                                          ),
                                        ),
                                        Text(
                                          _headimage.path.toString(),
                                        ),
                                      ],
                                    ),
                            ),
                            ButtonBar(
                              children: <Widget>[
                                RaisedButton(
                                  onPressed: () {
                                    getHeadImage();
                                  },
                                  color: Colors.blue[900],
                                  child: Text("Select Head Image"),
                                  elevation: 8.0,
                                ),
                                RaisedButton(
                                  onPressed: () {
                                    setState(() {
                                      _headimage = null;
                                    });
                                  },
                                  color: Colors.red[500],
                                  child: Text("Cancel Selection"),
                                  elevation: 8.0,
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                            right: 10.0, left: 10.0, bottom: 10.0),
                        child: Divider(
                          indent: 5.0,
                          thickness: 1.0,
                          color: Colors.grey[500],
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 16, right: 16, top: 2.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              "Select Event Date",
                              style: TextStyle(
                                  fontSize: 15,
                                  fontWeight: FontWeight.w500,
                                  color: Colors.black87),
                            ),
                            SizedBox(height: 10.0),
                            Container(
                              height: 15,
                              child: date == null
                                  ? Text(
                                      " Date is not Yet Selected ",
                                      style: TextStyle(
                                          fontWeight: FontWeight.w400,
                                          fontSize: 15),
                                    )
                                  : Align(
                                      alignment: Alignment.topLeft,
                                      child: Text(
                                        "Selected Date is : " + date.toString(),
                                        style: TextStyle(
                                            fontWeight: FontWeight.w400),
                                      ),
                                    ),
                            ),
                            ButtonBar(
                              children: <Widget>[
                                RaisedButton(
                                  onPressed: () async {
                                    final selectedDate =
                                        await _selectDateTime(context);

                                    if (selectedDate == null) return;

                                    final selectedTime =
                                        await _selectTime(context);
                                    if (selectedTime == null) return;
                                    setState(() {
                                      date = selectedDate;
                                    });
                                  },
                                  color: Colors.blue[900],
                                  child: Text("Select Event Date and Time"),
                                  elevation: 8.0,
                                ),
                                RaisedButton(
                                  onPressed: () {
                                    setState(() {
                                      date = null;
                                    });
                                  },
                                  color: Colors.red[500],
                                  child: Text("Cancel Selection"),
                                  elevation: 8.0,
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 10.0,
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(2.0),
              child: MaterialButton(
                minWidth: double.infinity,
                color: Colors.blue[900],
                height: 60,
                onPressed: () async {
                  await uploadCoverImage(_coverimage);
                  await uploadHeadImage(_headimage);
                  print("images Uploaded Successfully");
                  await createEvent(eventnamecontroller.text,
                      eventinfocontroller.text, eventheadnamecontroller.text);
                },
                shape: RoundedRectangleBorder(
                    side: BorderSide(color: Colors.black),
                    borderRadius: BorderRadius.circular(10)),
                child: Text(
                  "Add Event in Database",
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.w600,
                      fontSize: 18),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
