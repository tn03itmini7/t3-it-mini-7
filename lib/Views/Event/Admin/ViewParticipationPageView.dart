import 'dart:convert';
import 'package:EventsApp/Models/EventModel.dart';
import 'package:EventsApp/Models/UserModel.dart';
import 'package:EventsApp/Services/EventService.dart';
import 'package:EventsApp/Views/Common/NoItems.dart';
import 'package:EventsApp/Views/Event/Skeleton/ViewPartcipationSkeleton.dart';
import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';

class ViewParticipationPageView extends StatefulWidget {
  final EventModel event;
  ViewParticipationPageView({Key key, @required this.event}) : super(key: key);

  @override
  _ViewParticipationPageViewState createState() =>
      _ViewParticipationPageViewState();
}

class _ViewParticipationPageViewState extends State<ViewParticipationPageView> {
  List<UserModel> _users = List<UserModel>();
  String eventId;
  bool _isLoading = false;

  removeParicipant(String userId, String eventId) async {
    EventService eventService = new EventService();
    var res = await eventService.removeParticipantFromEvent(userId, eventId);
    if (res.statusCode == 200) {
      Get.snackbar(
        "   Participant Removed Successfully",
        "   Success ",
        margin: EdgeInsets.all(10.0),
        colorText: Colors.black,
        snackPosition: SnackPosition.TOP,
        backgroundColor: Colors.grey[100],
        duration: Duration(seconds: 2),
        icon: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(10), bottomLeft: Radius.circular(10)),
            color: Colors.black,
          ),
          width: 150,
          height: 80,
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Center(
              child: Icon(
                Icons.done_outline_rounded,
                color: Color.fromARGB(255, 255, 208, 0),
              ),
            ),
          ),
        ),
      );
    } else {
      Get.snackbar(
        "  Error in Removing Participant",
        "  Please Try Later",
        margin: EdgeInsets.all(10.0),
        colorText: Colors.black,
        snackPosition: SnackPosition.TOP,
        backgroundColor: Colors.grey[100],
        duration: Duration(seconds: 2),
        icon: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(10), bottomLeft: Radius.circular(10)),
            color: Colors.black,
          ),
          width: 150,
          height: 80,
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Center(
              child: Icon(
                Icons.error_outline,
                color: Color.fromARGB(255, 255, 208, 0),
              ),
            ),
          ),
        ),
      );
    }
  }

  Future<List<UserModel>> getParticipatedUsersinEvent() async {
    EventService eventService = new EventService();
    var res = await eventService.getParticipatedUsersEventId(widget.event.id);
    final parsedjson = json.decode(res.body);
    var userslist = parsedjson["body"] as List;
    var users = List<UserModel>();
    for (var user in userslist) {
      users.add(UserModel.fromJson(user));
    }
    setState(() {
      _isLoading = false;
    });
    return users;
  }

  @override
  void initState() {
    setState(() {
      _isLoading = true;
    });
    getParticipatedUsersinEvent().then((value) => {
          setState(() {
            _users.addAll(value);
          })
        });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          widget.event.name,
          style: TextStyle(color: Colors.white),
        ),
        backgroundColor: Colors.blue[900],
        elevation: 0,
        brightness: Brightness.light,
        leading: Icon(
          Icons.event,
          color: Colors.white,
        ),
        actions: <Widget>[
          Padding(
            padding: EdgeInsets.only(right: 20.0),
            child: Container(
              width: 35,
              height: 35,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  image: DecorationImage(
                      image: AssetImage('assets/images/Splash.png'),
                      fit: BoxFit.cover)),
              child: Transform.translate(
                offset: Offset(15, -15),
                child: Container(
                  margin: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                      border: Border.all(width: 3, color: Colors.white),
                      shape: BoxShape.circle,
                      color: Colors.yellow[800]),
                ),
              ),
            ),
          )
        ],
      ),
      body: _isLoading
          ? DelayedList()
          : _users.isEmpty
              ? NoItemsFoundWidget().noParticipantFoundWidget()
              : ListView.builder(
                  itemCount: _users.length,
                  itemBuilder: (context, index) {
                    return Card(
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          ListTile(
                            leading: const Icon(Icons.verified_user),
                            title: Text(_users[index].name),
                            subtitle: Text(_users[index].email),
                            trailing: new Column(
                              children: <Widget>[
                                new IconButton(
                                    icon: new Icon(Icons.close),
                                    color: Colors.redAccent[700],
                                    iconSize: 30,
                                    onPressed: () async {
                                      AwesomeDialog(
                                          context: context,
                                          dialogType: DialogType.WARNING,
                                          headerAnimationLoop: false,
                                          animType: AnimType.TOPSLIDE,
                                          title: 'Warn',
                                          desc:
                                              'Are You Sure you want to Remove this Participant ?',
                                          btnCancelOnPress: () {},
                                          btnOkOnPress: () async {
                                            await removeParicipant(
                                                _users[index].id,
                                                widget.event.id);
                                            setState(() {
                                              _users.removeAt(index);
                                            });
                                          }).show();
                                    })
                              ],
                            ),
                          ),
                        ],
                      ),
                    );
                  }),
    );
  }
}
