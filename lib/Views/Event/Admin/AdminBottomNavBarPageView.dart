import 'package:EventsApp/Views/Event/Admin/ActivateEvents.dart';
import 'package:EventsApp/Views/Event/Admin/AddEventPage.dart';
import 'package:EventsApp/Views/Event/Admin/AdminProfilePageView.dart';
import 'package:EventsApp/Views/Event/Admin/DeActivateEvents.dart';
import 'package:flutter/material.dart';

class AdminBottomNavBarPageView extends StatefulWidget {
  AdminBottomNavBarPageView({Key key}) : super(key: key);

  @override
  _AdminBottomNavBarPageViewState createState() =>
      _AdminBottomNavBarPageViewState();
}

class _AdminBottomNavBarPageViewState extends State<AdminBottomNavBarPageView> {
  int _selectedIndex = 0;

  void _onItemTap(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  List<Widget> _buildScreens = [
    AddEventPageView(),
    ActivateEventPageView(),
    DeActiavteEventPageView(),
    AdminProfilePageView(),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Material(
          child: _buildScreens.elementAt(_selectedIndex),
        ),
        bottomNavigationBar: BottomNavigationBar(
          selectedItemColor: Colors.white,
          unselectedItemColor: Colors.grey[400],
          backgroundColor: Colors.orangeAccent,
          items: [
            BottomNavigationBarItem(
              icon: Icon(Icons.event),
              backgroundColor: Colors.blue[900],
              label: "Add Event",
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.event_available),
              backgroundColor: Colors.blue[900],
              label: "Activate Events",
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.event_available_outlined),
              backgroundColor: Colors.blue[900],
              label: "View Participation",
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.settings),
              backgroundColor: Colors.blue[900],
              label: "Settings",
            ),
          ],
          currentIndex: _selectedIndex,
          onTap: _onItemTap,
        ));
  }
}
