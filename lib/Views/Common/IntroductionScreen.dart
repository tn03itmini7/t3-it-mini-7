
import 'package:EventsApp/Views/Common/SplashScreen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:introduction_screen/introduction_screen.dart';

class IntroScreenPageView extends StatelessWidget {
  IntroScreenPageView({Key key}) : super(key: key);

  final pageDecoration = PageDecoration(
    titleTextStyle:
        PageDecoration().titleTextStyle.copyWith(color: Colors.black),
    bodyTextStyle: PageDecoration().bodyTextStyle.copyWith(color: Colors.black),
    contentPadding: const EdgeInsets.all(10),
  );

  List<PageViewModel> getPages() {
    return [
      PageViewModel(
          image: Container(
            color: Colors.white,
            child: Padding(
              padding: const EdgeInsets.only(top: 100, right: 2.0, left: 2.0),
              child: Image.asset("assets/images/Splash.png"),
            ),
          ),
          titleWidget: Container(
              child: Text(
            "Attend Your Favorite Events",
            style: TextStyle(color: Colors.black, fontSize: 25),
          )),
          body: "All Categories Available.",
          decoration: pageDecoration),
      PageViewModel(
          image: Container(
            color: Colors.white,
            child: Padding(
              padding: const EdgeInsets.only(top: 100, right: 2.0, left: 2.0),
              child: Image.asset("assets/images/Splash.png"),
            ),
          ),
          titleWidget: Container(
              child: Text(
            "Attend Your Favorite Events",
            style: TextStyle(color: Colors.black, fontSize: 25),
          )),
          body: "All Categories Available.",
          decoration: pageDecoration),
      PageViewModel(
          image: Container(
            color: Colors.white,
            child: Padding(
              padding: const EdgeInsets.only(top: 100, right: 2.0, left: 2.0),
              child: Image.asset("assets/images/Splash.png"),
            ),
          ),
          titleWidget: Container(
              child: Text(
            "Attend Your Favorite Events",
            style: TextStyle(color: Colors.black, fontSize: 25),
          )),
          body: "All Categories Available.",
          decoration: pageDecoration),
      PageViewModel(
          image: Container(
            color: Colors.white,
            child: Padding(
              padding: const EdgeInsets.only(top: 100, right: 2.0, left: 2.0),
              child: Image.asset("assets/images/Splash.png"),
            ),
          ),
          titleWidget: Container(
              child: Text(
            "Attend Your Favorite Events",
            style: TextStyle(color: Colors.black, fontSize: 25),
          )),
          body: "All Categories Available.",
          decoration: pageDecoration),
    ];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: IntroductionScreen(
          showSkipButton: true,
          skipFlex: 0,
          nextFlex: 0,
          curve: Curves.bounceIn,
          animationDuration: 350,
          skip: const Text('Skip'),
          next: const Icon(Icons.arrow_forward),
          dotsDecorator: const DotsDecorator(
          size: Size(10.0, 10.0),
          color: Color(0xFFBDBDBD),
          activeSize: Size(22.0, 10.0),
          activeShape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(25.0)),
          )),
          pages: getPages(),
          onDone: () {
            Get.offAll(() => Splashscreen());
          },
          done: Text(
            "Done",
            style: TextStyle(color: Colors.black),
          ),
        ),
      ),
    );
  }
}
