import 'dart:async';
import 'package:EventsApp/Views/Authentication/LoginPageView.dart';
import 'package:EventsApp/Views/Event/Admin/AdminBottomNavBarPageView.dart';
import 'package:EventsApp/Views/Event/User/UserBottomNavBarPageView.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Splashscreen extends StatefulWidget {
  @override
  _SplashscreenState createState() => _SplashscreenState();
}

class _SplashscreenState extends State<Splashscreen> {
  startTime() async {
    var _duration = new Duration(seconds: 3);
    return new Timer(_duration, navigationPage);
  }

  void navigationPage() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var loginstatus = prefs.getBool('isLoggedIn');
    print(loginstatus);
    if (loginstatus == true) {
      var role = prefs.getString('role');
      if (role == 'User') {
        Get.offAll(() => UserBottomNavBarPageView());
      } else if (role == 'ADMIN') {
        Get.offAll(() => AdminBottomNavBarPageView());
      }
    } else {
      Get.offAll(() => LoginPageView());
    }
  }

  @override
  void initState() {
    super.initState();
    startTime();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.blue[900],
        body: Stack(
          fit: StackFit.expand,
          children: <Widget>[
            Container(decoration: BoxDecoration(color: Colors.blue[900])),
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Expanded(
                  flex: 2,
                  child: Container(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Image.asset('assets/images/Splash.png'),
                        Padding(
                          padding: EdgeInsets.only(top: 10.0),
                        ),
                        Text(
                          "Events App",
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 24.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Expanded(
                    flex: 1,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        CircularProgressIndicator(
                          valueColor:
                              new AlwaysStoppedAnimation<Color>(Colors.white),
                        ),
                        Padding(padding: EdgeInsets.only(top: 20.0)),
                      ],
                    ))
              ],
            )
          ],
        ));
  }
}
