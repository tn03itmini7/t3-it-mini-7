import 'package:flutter/material.dart';

class NoItemsFoundWidget{

  Widget noEventsFoundWidget(){
    return Center(
      child: Container(
        alignment: Alignment.center,
        color: Colors.white,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 25.0, right: 25),
              child: Image.asset('assets/images/Splash.png'),
            ),
            SizedBox(
              height: 15,
            ),
            Text("Sorry! No Events Found",
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
          ],
        ),
      ),
    );
  }
  
  Widget noNearByEventsFoundWidget(){
    return Center(
      child: Container(
        alignment: Alignment.center,
        color: Colors.white,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 25.0, right: 25),
              child: Image.asset('assets/images/noactive.png'),
            ),
            SizedBox(
              height: 15,
            ),
            Text("Sorry! No NearByEvents Found",
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
          ],
        ),
      ),
    );
  }

  Widget noDeActiveEventsFoundWidget(){
    return Center(
      child: Container(
        alignment: Alignment.center,
        color: Colors.white,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 25.0, right: 25),
              child: Image.asset('assets/images/noactive.png'),
            ),
            SizedBox(
              height: 15,
            ),
            Text("Sorry! No DeActive Events Found",
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
          ],
        ),
      ),
    );
  }

  Widget noActiveEventsFoundWidget(){
    return Center(
      child: Container(
        alignment: Alignment.center,
        color: Colors.white,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 25.0, right: 25),
              child: Image.asset('assets/images/no-event-found.png'),
            ),
            SizedBox(
              height: 15,
            ),
            Text("Sorry! No Active Events Found",
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
          ],
        ),
      ),
    );
  }

  Widget noParticipantFoundWidget(){
    return Center(
      child: Container(
        alignment: Alignment.center,
        color: Colors.white,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 25.0, right: 25),
              child: Image.asset('assets/images/Splash.png'),
            ),
            SizedBox(
              height: 15,
            ),
            Text("Sorry! No Participants Found for this Event",
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
          ],
        ),
      ),
    );
  }



}