import 'dart:async';
import 'dart:convert';

import 'package:EventsApp/Constants/Environment.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;

class UserService {
  //login
  Future<http.Response> login(String email, String password) async {
    return await http
        .get(Environment().apiurl + '/user/login/' + email + '/' + password);
  }

  //Fetch User Favorites
  Future<http.Response> getUserFavorites(String userId) async {
    return await http
        .get(Environment().apiurl + '/user/fetchuserfav/' + userId);
  }

  //Register New User
  Future<http.Response> signup(String name, String email, String pass) async {
    return await http.post(Environment().apiurl + '/user/add',
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode(
            <String, String>{'name': name, 'email': email, 'password': pass}));
  }
}
